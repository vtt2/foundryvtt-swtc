#!/bin/bash

set -euo pipefail
set -x

#npm install --save-dev babel-eslint

cat > .eslintrc.js << EOF 
module.exports = {
  parser: "/usr/local/lib/node_modules/@babel/eslint-parser",
  parserOptions: {
    sourceType: "module",
    allowImportExportEverywhere: false,
    ecmaFeatures: {
      globalReturn: false,
    },
    requireConfigFile: false,
  },
};
EOF

#npx eslint ./src/
eslint ./swtextcrawl.js

# Locate and complain about extraneous console logging
if grep 'console.log(' swtextcrawl.js; then
  echo "console.log found, exiting..."
  exit 1
fi
