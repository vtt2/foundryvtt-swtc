#!/bin/bash

set -euo pipefail
set -x

if echo "${CI_COMMIT_TAG}"|grep -E '^(v|rc-|alpha-|beta-)[0-9]+\.[0-9]+\.[0-9]+'; then
  if echo "${CI_COMMIT_TAG}"|grep -E '^v'; then
    BASE="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/swtextcrawl"
  else
    POST="$(echo ${CI_COMMIT_TAG}|sed 's/^\([a-z]*\)-.*/\1/')"
    BASE="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/swtextcrawl-${POST}"
  fi
  VERSION=$(echo "${CI_COMMIT_TAG}" | sed 's/[^0-9.]*\([0-9.]*\).*/\1/')
  REL="${BASE}/${VERSION}"
  ZIP="${REL}/swtextcrawl.zip"
  MANIFEST="${REL}/module.json"
  sed -i "s/\"version\":.*/\"version\": \"${VERSION}\",/" module.json
  sed -i "s#\"manifest\":.*#\"manifest\": \"${MANIFEST}\",#" module.json
  sed -i "s#\"download\":.*#\"download\": \"${ZIP}\",#" module.json
fi

apt-get update -y
apt-get install zip -y
mkdir -p .build/swtextcrawl
cp -r * .build/swtextcrawl
mv .build build
mkdir -p build/artifacts
cd build
zip -r artifacts/swtextcrawl.zip swtextcrawl/
ls -al artifacts
