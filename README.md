# Space Wars Text Crawl Module

## Description
This module allows the GM to create a Text Crawl using a journal entry page.
If you name a journal page with the words "Text Crawl" in the title a new context button will appear if you right-click the page in the left sidebar.
The context button, "Show Crawl", will play a crawl to all players from the properly formatted Journal.

## Usage

You must craft a journal page that has a title that contains the words "Text Crawl", such as "First Text Crawl".
For the "Episode" text use the h2 header.
For the "Title" text use the h3 header.
Use ordinary paragraph text for the body of the crawl.
Putting an image into the journal after the text (such as of a planet) will show the image after a pan-down the end of the crawl.

To actually trigger the crawl click first on the journal page on the left-hand side, then right-click and choose the option "Show Crawl to Players".
This option will only appear if the journal page has a title with "Text Crawl" in it.
For _reasons_ you must left-click the page first on the left sidebar before it will activate the context menu on the correct page.

The included compendium has a sample journal entry that can be used to see how it works.

## Configuration
The configuration menu has some options that can be tweaked.
The hints describe what they do.
Using them you can set a logo image (recommended png or webp with a transparent background), intro music, etc.

## Examples
<img src="https://gitlab.com/vtt2/foundryvtt-swtc/-/wikis/uploads/17946d683ed2d9c317c71eb23f8a90b1/readme1.png" alt="README1">

### A short video showing it in action: [_link_](https://gitlab.com/vtt2/foundryvtt-swtc/-/wikis/uploads/7151aa554cbe699075433db28098fe43/swtextcrawl.mp4)

## Credits

LostPhoenix for the original Space Wars Text Crawl.

A lot of code was cribbed from [FFG Star Wars Enhancements](https://foundryvtt.com/packages/ffg-star-wars-enhancements).
Many thanks to them for having already incorporated the code from [Kessel Labs](https://kassellabs.io/).

## Fonts
### Star Jedi
| Author | Boba Fonts |
| Year | 1998 |
| License | [Freeware](https://www.fontspace.com/star-jedi-font-f9641) |

### News Cycle
| Author | Nathan Willis |
| Year | 2010-2011 |
| License | OFL |

See `NewsCycle-OFL.txt` for additional details.

### SW Crawl Title
https://www.fontcrown.com/fonts/9198/sw_crawl_title.html
