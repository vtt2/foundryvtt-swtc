/**
 * Register settings used by the opening crawl.
 */
export function crawlInit() {
    game.settings.register("swtextcrawl", "intro-1", {
        module: "swtextcrawl",
        name: game.i18n.localize("swtextcrawl.intro-1"),
        hint: game.i18n.localize("swtextcrawl.intro-1-hint"),
        scope: "world",
        config: true,
        type: String,
        default: "A little while ago in a place far,",
    });
    game.settings.register("swtextcrawl", "intro-2", {
        module: "swtextcrawl",
        name: game.i18n.localize("swtextcrawl.intro-2"),
        hint: game.i18n.localize("swtextcrawl.intro-2-hint"),
        scope: "world",
        config: true,
        type: String,
        default: "far away...",
    });
    game.settings.register("swtextcrawl", "music", {
        module: "swtextcrawl",
        name: game.i18n.localize("swtextcrawl.music"),
        hint: game.i18n.localize("swtextcrawl.music-hint"),
        scope: "world",
        config: true,
        type: String,
        filePicker: "audio",
        default: "",
    });
    game.settings.register("swtextcrawl", "music_volume", {
        module: "swtextcrawl",
        name: game.i18n.localize("swtextcrawl.music-volume"),
        hint: game.i18n.localize("swtextcrawl.music-volume-hint"),
        scope: "client",
        config: true,
        type: Number,
        filePicker: "audio",
        default: 25,
    });
    game.settings.register("swtextcrawl", "logo", {
        module: "swtextcrawl",
        name: game.i18n.localize("swtextcrawl.logo"),
        hint: game.i18n.localize("swtextcrawl.logo-hint"),
        scope: "world",
        config: true,
        type: String,
        filePicker: "image",
        default: "",
    });
    game.settings.register("swtextcrawl", "logo-text", {
        module: "swtextcrawl",
        name: game.i18n.localize("swtextcrawl.logo-text"),
        hint: game.i18n.localize("swtextcrawl.logo-text-hint"),
        scope: "world",
        config: true,
        type: String,
        default: "<div>SPACE</div><div>WARS</div>",
    });
    game.settings.register("swtextcrawl", "music-delay", {
        module: "swtextcrawl",
        name: game.i18n.localize("swtextcrawl.music-delay"),
        hint: game.i18n.localize("swtextcrawl.music-delay-hint"),
        scope: "world",
        config: true,
        type: Number,
        default: 0.0,
    });
    game.settings.register("swtextcrawl", "image-right", {
        module: "swtextcrawl",
        name: game.i18n.localize("swtextcrawl.image-right"),
        hint: game.i18n.localize("swtextcrawl.image-right-hint"),
        scope: "world",
        config: true,
        type: Number,
        default: 0,
    });
    game.settings.register("swtextcrawl", "image-bottom", {
        module: "swtextcrawl",
        name: game.i18n.localize("swtextcrawl.image-bottom"),
        hint: game.i18n.localize("swtextcrawl.image-bottom-hint"),
        scope: "world",
        config: true,
        type: Number,
        default: 1300,
    });
    game.settings.register("swtextcrawl", "font-size", {
        module: "swtextcrawl",
        name: game.i18n.localize("swtextcrawl.font-size"),
        hint: game.i18n.localize("swtextcrawl.font-size-hint"),
        scope: "world",
        config: true,
        type: Number,
        default: 350,
    });
}

/**
 * Application that displays the Opening Crawl.
 */
class SpaceOperaTextCrawlApplication extends Application {
    /**
     * @param {object} data object to provide the opening crawl template
     * @param {object} options additional options for the application
     */
    constructor(data, options) {
        super({}, options);
        this.data = data;
    }

    /**
     * Configure a "full" screen with minimal controls that will display the
     * Opening Crawl.
     */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            template: "modules/swtextcrawl/templates/opening_crawl.html",
            id: "opening-crawl",
            title: game.i18n.localize("swtextcrawl.title"),
            minimizable: false,
            editable: false,
            resizable: true,
            popOut: true,
            shareable: false,
            top: 0,
            left: 0,
            width: 4096,
            height: 2160,
        });
    }

    /**
     * Provide the data object for the template.
     * @returns object provided to the constructor
     */
    getData() {
        let data = this.data;
        data.img = {};
        data.img.bottom = game.settings.get("swtextcrawl", "image-bottom");
        data.img.right = game.settings.get("swtextcrawl", "image-right");
        data.size = game.settings.get("swtextcrawl", "font-size");
        return data;
    }

    /**
     * Play this.data.music using the Playlist volume.
     * @param {function} callback callback to execute once playback has started
     */
    async play_music(callback) {
        let coreVolume =  game.settings.get("core", "globalPlaylistVolume");
        let volume = coreVolume * (game.settings.get("swtextcrawl", "music_volume")/100);
        let audio_helper = game.audio;
        const that = this;

        audio_helper.preload(this.data.music).then(async (sound) => {
            callback();
            await sleep(game.settings.get("swtextcrawl", "music-delay"));
            sound.play({
                volume: volume,
            });
            that.sound = sound;
        });
    }

    /**
     * Listener that times the audio playing the audio with the opening crawl.
     * @param {jQuery} html
     */
    activateListeners(html) {
        super.activateListeners(html);

        // When music is configured, hide the HTML until the audio loaded is
        // loaded. Once it's loaded, redisplay the HTML to retrigger the
        // animation.
        if (this.data.music) {
            function start_animation() {
                html[0].style.display = "block";
            }

            html[0].style.display = "none";

            this.play_music(start_animation);
        }

        if(this.data.extra > 0) {
            document.documentElement.style.setProperty("--extra-time", this.data.extra +'s');
            const p3 = 1+(Math.floor(this.data.extra/73))*2;
            const p1 = (1+this.data.extra/73)/100;
            const bezier = "cubic-bezier(" + p1 + "," + "0" + "," + "1" + "," + p3 + ")";
            document.documentElement.style.setProperty("--intro-crawl-ease", bezier);
        }
    }

    close() {
        if (this.sound) {
            this.sound.stop();
            this.sound = null;
        }
        return super.close();
    }
}

/**
 * Ready handler that listens on the swtextcrawl socket.
 */
export function ready() {
    game.socket.on("module.swtextcrawl", socket_listener);
}

/**
 * Parse the contents of a journal into the parts of the Opening Crawl into an
 * object.
 * @param  journal_html
 * @returns {object|null}
 */
function parse_journal(journal_html) {
    let episode_html = journal_html.find("h2");
    let title_html = journal_html.find("h3");
    let extraTime = journal_html.find("h6")
    let extra = 0

    if (episode_html.length === 0) {
        ui.notifications.warn(game.i18n.localize("swtextcrawl.missing-episode"));
        return null;
    }
    if (title_html.length === 0) {
        ui.notifications.warn(game.i18n.localize("swtextcrawl.missing-title"));
        return null;
    }

    let image = null;
    let image_html = journal_html.find("img");
    if (image_html.length) {
        image = image_html.attr("src");
    }

    let body = journal_html
        .find("p")
        .map(function () {
            return $(this).text();
        })
        .toArray();

    if (body.length === 0) {
        ui.notifications.warn(game.i18n.localize("swtextcrawl.missing-body"));
        return null;
    }

    if(extraTime.text().length > 0) {
        extra = parseInt(extraTime.text());

        if (!Number.isInteger(extra)) {
            ui.notifications.warn(game.i18n.localize('swtextcrawl.duration-nan'));
        }
    }

    return {
        episode: episode_html.text(),
        title: title_html.text(),
        body: body,
        image: image,
        extra: extra
    };
}

/**
 * Listener for the swtextcrawl socket that launches the
 * SpaceOperaTextCrawlApplication if the message type is "opening-crawl"
 *
 * @param {object} data object passed to SpaceOperaTextCrawl
 */
function socket_listener(data) {
    if (data.type === "opening-crawl") {
        new SpaceOperaTextCrawlApplication(data).render(true);
    }
}

export function launch_opening_crawl(data) {
    data = mergeObject(data, {
        type: "opening-crawl",
        logo: game.settings.get("swtextcrawl", "logo"),
        music: game.settings.get("swtextcrawl", "music"),
    });
    game.socket.emit("module.swtextcrawl", data);
    socket_listener(data);
}

/**
 * Helper function to delay code execution by an arbitrary amount
 * @param ms - number of MS to delay by
 * @returns {Promise<unknown>}
 */
function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

Hooks.once("init", async function() {
    crawlInit();
    Handlebars.registerHelper('swtextcrawlGetConfig', function(config) {
        return game.settings.get('swtextcrawl', config);
    })
});

Hooks.once("ready", () => {
    ready();
})

Hooks.on("getJournalSheetEntryContext", (journal, options) => {

    if(game.user.isGM) {
        const button = {
            name: "swtextcrawl.show-crawl",
            icon: '<i class="fa-solid fa-film"></i>',
            condition: li => {
              return li[0].innerText.includes("Text Crawl");
            },
            callback: () => {
                const crawlData = parse_journal($("<div/>").append($(journal._pages[journal.pageIndex].text.content)));
                if (!crawlData) {
                    return false;
                }

                return launch_opening_crawl(crawlData);
            },
        }
        options.push(button);
    }
})